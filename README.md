# BtBlogUi
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.25. and was developed using Npm v 6.9.0.

Some things be aware about this version:  
- The main intention of this project is to show knowledge of the platform and common tasks for UI development. 
- Source code isn't production ready, as I would usually use a state management library instead of calling directly a service from a component.
- Test coverage is low as I wanted to show that I know how to test simple components (i.e. pipes) and a service using a HTTP call.
- Look and feel is using a basic Material Angular theme with very little customisations. 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Application running

![Image description](application-running.png)

