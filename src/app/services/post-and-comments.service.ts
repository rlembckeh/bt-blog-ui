import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {CommentDTO, UserDTO} from '../models/post-and-comments.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostAndCommentsService {

  USER_AND_POSTS_URI = environment.baseApi + '/usersAndPosts';
  COMMENTS_URI = environment.baseApi + '/comments';

  constructor(private http: HttpClient) {
  }


  getUsersAndPosts(): Observable<UserDTO[]> {
    return this.http.get<UserDTO[]>(this.USER_AND_POSTS_URI)
      .pipe(
        catchError(this.handleError<UserDTO[]>('getUsersAndPosts', []))
      );
  }

  getComments(postId: string): Observable<CommentDTO[]> {
    const params: HttpParams = new HttpParams().set('postId', postId);
    return this.http.get<CommentDTO[]>(this.COMMENTS_URI, {params})
      .pipe(
        catchError(this.handleError<CommentDTO[]>('getComments', []))
      );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console
      return of(result as T);
    };
  }


}


