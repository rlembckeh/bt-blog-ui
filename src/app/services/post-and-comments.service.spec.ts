import {getTestBed, inject, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import {PostAndCommentsService} from './post-and-comments.service';
import {UserDTO} from '../models/post-and-comments.model';

describe('PostAndCommentsService', () => {

  let injector: TestBed;
  let service: PostAndCommentsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PostAndCommentsService]
    });
    injector = getTestBed();
    service = injector.get(PostAndCommentsService);
    httpMock = injector.get(HttpTestingController);
  });


  describe('getUsersAndPosts()', () => {

    it('should return an Observable<User[]>', inject([PostAndCommentsService], (postAndCommentsService) => {
      const mockResponse: UserDTO [] = [
        {name: 'David', username: 'david', id: 1},
        {name: 'Romulo', username: 'romulo', id: 2}
      ];

      service.getUsersAndPosts().subscribe(users => {
        expect(users.length).toBe(2);
      });

      const req = httpMock.expectOne(service.USER_AND_POSTS_URI);
      expect(req.request.method).toBe('GET');
      req.flush(mockResponse);


    }));

  });


});
