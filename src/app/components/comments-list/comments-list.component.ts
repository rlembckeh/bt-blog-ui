import {Component, Input, OnInit} from '@angular/core';
import {CommentDTO} from '../../models/post-and-comments.model';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.css']
})
export class CommentsListComponent implements OnInit {

  @Input()
  comments: CommentDTO[];

  constructor() { }

  ngOnInit() {
  }

}
