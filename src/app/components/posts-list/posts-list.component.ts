import {Component, Input, OnDestroy} from '@angular/core';
import {CommentDTO, UserDTO} from '../../models/post-and-comments.model';
import {PostAndCommentsService} from '../../services/post-and-comments.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnDestroy {

  DEFAULT_POST_SHOW = 3;
  isLoading = false;
  showAll = false;
  expanded: number;
  // tslint:disable-next-line:variable-name
  _selectedUser: UserDTO;
  comments: CommentDTO[];

  private subscription: Subscription;

  constructor(private postAndCommentsService: PostAndCommentsService) {
  }

  @Input()
  set selectedUser(value: UserDTO) {
    this._selectedUser = value;
    this.showAll = false;
  }

  get selectedUser(): UserDTO {
    return this._selectedUser;
  }

  showAllPosts() {
    this.showAll = true;
  }

  showComments(postId: number) {
    this.expanded = postId;
    this.isLoading = true;
    this.comments = [];
    this.subscription = this.postAndCommentsService.getComments(postId.toString())
      .subscribe(
        (comments: CommentDTO[]) => {
          this.comments = comments;
        },
        (err) => {
          this.isLoading = false;
          console.log(err);
        }, // Here I would use a different log strategy for prod code
        () => this.isLoading = false
      );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
