import {Component, OnDestroy, OnInit} from '@angular/core';
import {PostAndCommentsService} from '../../services/post-and-comments.service';
import {UserDTO} from '../../models/post-and-comments.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit, OnDestroy {

  usersAndPosts: UserDTO[];
  selectedUser: UserDTO;
  isLoading = true;

  private subscription: Subscription;

  constructor(private postAndCommentsService: PostAndCommentsService) {
  }


  ngOnInit() {
    this.subscription = this.postAndCommentsService.getUsersAndPosts().subscribe(
      (users: UserDTO[]) => {
        this.isLoading = false;
        return this.usersAndPosts = users;
      }
    );
  }

  onSelect(user: UserDTO): void {
    this.selectedUser = user;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
