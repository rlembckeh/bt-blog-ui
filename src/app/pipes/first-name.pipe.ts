import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'firstNamePipe'
})
export class FirstNamePipe implements PipeTransform {

  // Rudimentary implementation for a first name extractor
  transform(value: string): string {
    if (value == null) {
      return value;
    }
    value = value.trim();
    const words = value.split(' ');
    if (words.length > 0) {

      if (this.skipFirstWord(words[0]) && words.length > 1) {
        return words[1];
      } else {
        return words[0];
      }

    } else {
      return value;
    }

  }


  private skipFirstWord(firstWord: string): boolean {
    return 'Mr.Mrs.Ms.Dr.'.includes(firstWord);
  }


}
