import {FirstNamePipe} from './first-name.pipe';

describe('FirstNamePipe', () => {

  it('Handle wrong input', () => {
    const pipe = new FirstNamePipe();
    expect('').toEqual(pipe.transform(''));
  });

  it('Extract first word only', () => {
    const pipe = new FirstNamePipe();
    expect('Nicholas').toEqual(pipe.transform('Nicholas Runolfsdottir V'));
  });

  it('Extract first word only with blank spaces in front', () => {
    const pipe = new FirstNamePipe();
    expect('Nicholas').toEqual(pipe.transform('  Nicholas Runolfsdottir V'));
  });

  it('Ignore titles from name', () => {
    const pipe = new FirstNamePipe();
    expect('Romulo').toEqual(pipe.transform('Mr. Romulo Lembcke'));
  });
});
