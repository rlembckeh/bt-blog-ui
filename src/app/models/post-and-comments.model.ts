export interface UserDTO {
  id: number;
  username: string;
  name: string;
  posts?: PostDTO[];
}

export interface PostDTO {
  id: number;
  userId: number;
  title: string;
  body: string;
  comments?: CommentDTO[];
}

export interface CommentDTO {
  id: number;
  postId: number;
  name: string;
  email: string;
  body: string;
}
